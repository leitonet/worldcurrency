package com.example.worldcurrencies;


import java.io.Serializable;

class CurrencyModel implements Serializable{

    private double usd;
    private double eur;
    private double rub;
    private double pon;
    private double cny;

    public CurrencyModel(double usd, double eur, double rub, double pon, double cny) {
        this.usd = usd;
        this.eur = eur;
        this.rub = rub;
        this.pon = pon;
        this.cny = cny;
    }

    public double getUsd() {
        return usd;
    }

    public double getEur() {
        return eur;
    }

    public double getRub() {
        return rub;
    }

    public double getPon() {
        return pon;
    }

    public double getCny() {
        return cny;
    }
}

