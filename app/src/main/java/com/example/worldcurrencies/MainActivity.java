package com.example.worldcurrencies;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements СurrencyAdapter.CurrencyClickListener{

    public static final String KEY = "currency";
    public static final String FILE_NAME = "my_file";
    RecyclerView recyclerView;
    List<CurrencyModel> currencyModelList = new ArrayList<>();
    СurrencyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);

        adapter = new СurrencyAdapter(this, currencyModelList, this);
        recyclerView.setAdapter(adapter);

        if(fileExistance(FILE_NAME)){
            readData();
        }else {
            getData();
        }
    }

//    private List<CurrencyModel> createCurrencies() {
//        List<CurrencyModel> listOfflineCurrencies = new ArrayList<>();
//        listOfflineCurrencies.add(new CurrencyModel("USD", "EUR", "RUB", "PON", "CNY"));
//        return listOfflineCurrencies;
//    }

    private void getData() {
        Retrofit.getCurrencies(new Callback<List<CurrencyModel>>() {
            @Override
            public void success(List<CurrencyModel> currentCountry, Response response) {
                // listOfCountries = countries;
                updateUI(currentCountry);
                saveData(currentCountry);
                Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getKind() == RetrofitError.Kind.HTTP) {
                    Toast.makeText(getApplicationContext(), "HTTP error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveData(List<CurrencyModel> currentCountry) {
        FileOutputStream fos = null; //open stream for writing data to file_name file
        try {
            fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            String json = new Gson().toJson(currentCountry); //generate json
            fos.write(json.getBytes()); //write bites array
            fos.close(); //important: close stream
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private void readData() {
        StringBuilder json = new StringBuilder();
        try {
            FileInputStream fis = openFileInput(FILE_NAME);//open stream for reading data from file_name file
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis)); //create reader from stream
            String line;
            while ((line = reader.readLine()) != null) { // get text line by line, while text presen
                json.append(line); // save text line to StringBuilder
            }
            fis.close(); //important: close reader
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<ArrayList<CurrencyModel>>(){}.getType();
        List<CurrencyModel> countriesList = new Gson().fromJson(json.toString(), listType);
        updateUI(countriesList);
    }

    @Override
    public void onClick(CurrencyModel currencyModel) {
        Intent intent = new Intent(this, CurrencyModel.class);
        intent.putExtra(KEY, currencyModel);
        startActivity(intent);
    }

    private void updateUI(List<CurrencyModel> countries){
        currencyModelList.addAll(countries);
        adapter.notifyDataSetChanged();
    }

    private boolean fileExistance(String fname){
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }
}

