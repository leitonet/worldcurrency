package com.example.worldcurrencies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

 class СurrencyAdapter extends RecyclerView.Adapter<СurrencyAdapter.ViewHolder> {

    interface CurrencyClickListener {
        void onClick(CurrencyModel currencyModel);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<CurrencyModel> list;
    private CurrencyClickListener currencyClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            currencyClickListener.onClick(list.get(position));
        }
    };

    public СurrencyAdapter(Context context, List<CurrencyModel> list, CurrencyClickListener currencyClickListener) {
        this.context = context;
        this.list = list;
        this.currencyClickListener = currencyClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CurrencyModel currencyModel = list.get(position);
        holder.name.setText(currencyModel.getEur() + "");
        holder.fullName.setText(currencyModel.getUsd() + "");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView fullName;

        ItemClickListener itemClickListener;

        public ViewHolder(View item, final ItemClickListener itemClickListener) {
            super(item);

            this.itemClickListener = itemClickListener;

            name = (TextView) item.findViewById(R.id.name);
            fullName = (TextView) item.findViewById(R.id.full_name);


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }

}

