package com.example.worldcurrencies;


import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;


public class Retrofit {

    public static final String ENDPOINT = "http://api.fixer.io";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/latest")
        void getCountries(Callback<List<CurrencyModel>> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)

                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCurrencies(Callback<List<CurrencyModel>> callback) {
        apiInterface.getCountries(callback);
    }
}
